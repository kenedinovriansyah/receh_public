
import {AppRegistry, LogBox} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import 'react-native-gesture-handler';

LogBox.ignoreLogs(['VirtualizedLists should never be nested']);

AppRegistry.registerComponent(appName, () => App);
