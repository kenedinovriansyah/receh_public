import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import ChangePasswordComponent from '../../screen/auth/child/changePassword.component';
import ChangeEmailComponent from '../../screen/auth/child/childSettings/changeEmail.component';
import ProfileSettingsComponent from '../../screen/auth/child/childSettings/profileSettings.component';
import SettingsComponent from '../../screen/auth/child/settings.component';

type ParamsListChild = {
  default: any;
  profile: any;
  change_email: any;
  change_password: any
};

export type RouteListPathProp = 'default' | 'profile' | 'change_email' | 'change_password';

const RootStack = createStackNavigator<ParamsListChild>();

const NavigationChildSettings: React.FC = () => {
  return (
    <RootStack.Navigator>
      <RootStack.Screen
        name="default"
        component={SettingsComponent}
        options={{
          headerTitle: 'Back',
        }}
      />
      <RootStack.Screen
        name="profile"
        component={ProfileSettingsComponent}
        options={{
          headerTitle: 'Back',
        }}
      />
      <RootStack.Screen
        name="change_email"
        component={ChangeEmailComponent}
        options={{
          headerTitle: 'Back',
        }}
      />
      <RootStack.Screen
        name="change_password"
        component={ChangePasswordComponent}
        options={{
          headerTitle: "Back"
        }}/>
    </RootStack.Navigator>
  );
};

export default NavigationChildSettings;
