import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import LoginComponent from '../screen/auth/login.component';
import CreateComponent from '../screen/auth/create.component';
import ForgotComponent from '../screen/auth/forgot.component';
import TemplateComponent from '../screen/dating/template.component';
import {AuthContext} from '../screen/auth/context/ContextAuth';
import {useDispatch, useSelector} from 'react-redux';
import {UserTypes} from '../store/types/userTypes';
import {ApplicationState} from '../store/configureStore';
import ProfileComponent from '../screen/auth/child/profile.component';
import NavigationChildSettings from './child/childSettings.router';
import {RouteListPathProp} from './child/childSettings.router';

export type ParamsListProps = {
  login: undefined;
  register: undefined;
  forgot: undefined;
  dating: undefined;
  profile: {
    name: string;
  };
  settings: {
    screen: RouteListPathProp;
  };
};

export type RoutePath =
  | 'login'
  | 'register'
  | 'forgot'
  | 'dating'
  | 'profile'
  | 'settings';

const RootStack = createStackNavigator<ParamsListProps>();

const Routes: React.FC = () => {
  const userToken = useSelector((state: ApplicationState) => state.user.token);
  const dispatch = useDispatch();

  const authToken = React.useMemo(
    () => ({
      signIn: async (args: any) => {
        await dispatch({
          type: UserTypes.LoginIn,
          payload: {
            token: 'Token',
          },
        });
      },
      // tslint:disable-next-line:no-empty
      signOut: async () => {},
    }),
    [],
  );
  return (
    <AuthContext.Provider value={authToken}>
      {Boolean(userToken) ? (
        <RootStack.Navigator initialRouteName="dating">
          <RootStack.Screen
            name="dating"
            component={TemplateComponent}
            options={{
              headerShown: true,
              headerTitle: 'Graf',
              headerTintColor: 'white',
              headerTitleStyle: {
                alignSelf: 'center',
              },
              headerStyle: {
                backgroundColor: '#00b09b',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
                elevation: 5,
              },
            }}
          />
          <RootStack.Screen
            name="profile"
            component={ProfileComponent}
            options={({route}) => ({
              headerTitle: route.params.name,
            })}
          />
          <RootStack.Screen
            name="settings"
            component={NavigationChildSettings}
            options={({route}) => ({
              headerShown: false,
            })}
          />
        </RootStack.Navigator>
      ) : (
        <RootStack.Navigator initialRouteName="login">
          <RootStack.Screen
            name="login"
            component={LoginComponent}
            options={{
              headerShown: false,
              animationEnabled: false,
            }}
          />
          <RootStack.Screen
            name="register"
            component={CreateComponent}
            options={{
              headerShown: false,
              animationEnabled: false,
            }}
          />
          <RootStack.Screen
            name="forgot"
            component={ForgotComponent}
            options={{
              headerShown: false,
              animationEnabled: false,
            }}
          />
        </RootStack.Navigator>
      )}
    </AuthContext.Provider>
  );
};

export default Routes;
