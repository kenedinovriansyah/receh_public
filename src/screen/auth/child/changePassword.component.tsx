import {Button, Text, View} from 'native-base';
import React from 'react';
import {
  NativeSyntheticEvent,
  SafeAreaView,
  TextInput,
  TextInputChangeEventData,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {ChangePasswordState} from '../../../store/types/child/interface';
import {profileSettingsStyle} from './childSettings/assets/settings';

const ChangePasswordComponent: React.FC = () => {
  const [state, setState] = React.useState<ChangePasswordState>({
    oldPassword: '',
    newPassword: '',
    confirmPassword: '',
  });
  const onChangeOldPassword = (
    args: NativeSyntheticEvent<TextInputChangeEventData>,
  ) => {
    setState({
      ...state,
      oldPassword: args.nativeEvent.text,
    });
  };
  const onChangeNewPassword = (
    args: NativeSyntheticEvent<TextInputChangeEventData>,
  ) => {
    setState({
      ...state,
      newPassword: args.nativeEvent.text,
    });
  };
  const onChangeConfirmPassword = (
    args: NativeSyntheticEvent<TextInputChangeEventData>,
  ) => {
    setState({
      ...state,
      confirmPassword: args.nativeEvent.text,
    });
  };
  const onPress = () => {
    console.log(state);
  };
  return (
    <SafeAreaView style={{flex: 1}}>
      <KeyboardAwareScrollView>
        <View style={profileSettingsStyle.root}>
          <Text style={profileSettingsStyle.title}>Change Password</Text>
          <View style={profileSettingsStyle.ctxInput}>
            <TextInput
              secureTextEntry={true}
              placeholder="Old Password"
              value={state.oldPassword}
              onChange={onChangeOldPassword}
            />
          </View>
          <View style={profileSettingsStyle.ctxInput}>
            <TextInput
              secureTextEntry={true}
              placeholder="Old Password"
              value={state.newPassword}
              onChange={onChangeNewPassword}
            />
          </View>
          <View style={profileSettingsStyle.ctxInput}>
            <TextInput
              placeholder="Confirm Password"
              secureTextEntry={true}
              value={state.confirmPassword}
              onChange={onChangeConfirmPassword}
            />
          </View>
          <Button style={profileSettingsStyle.button} onPress={onPress}>
            <Text style={profileSettingsStyle.textButton}>Save</Text>
          </Button>
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

export default ChangePasswordComponent;
