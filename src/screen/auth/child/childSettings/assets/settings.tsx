import { Dimensions, StyleSheet } from "react-native";

export const profileSettingsStyle = StyleSheet.create({
    root: {
        display: 'flex',
        flexDirection: 'column'
    },
    title: {
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 20,
        marginBottom: 20
    },
    ctxAvatar: {
        width: 120,
        height: 120,
        borderRadius: 100,
        alignSelf: 'center',
        marginTop: 18,
        padding: 10,
        backgroundColor: 'white',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 8,
        marginBottom: 10
    },
    avatar: {
        width: 120,
        height: 120,
    },
    ctxInput: {
        backgroundColor: 'white',
        marginBottom: 4,
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 10,
        paddingLeft: 10,
        paddingRight: 10,
        // elevation: 1,
        borderWidth: 3,
        borderTopColor: 'transparent',
        borderBottomColor: 'transparent',
        borderRightColor: '#00b09b',
        borderLeftColor: '#00b09b'
    },
    textarea: {
        justifyContent: 'flex-start',
        overflow: 'scroll',
        height: 120
    },
    button: {
        backgroundColor: '#00b09b',
        width: 150,
        alignSelf: 'center',
        marginTop: 10,
        elevation: 10,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    textButton: {
        fontSize: 20,
        fontWeight: 'bold',
        textTransform: 'capitalize'
    }
})