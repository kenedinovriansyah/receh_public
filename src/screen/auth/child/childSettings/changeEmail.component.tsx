import {Button, Text, View} from 'native-base';
import React from 'react';
import {
  NativeSyntheticEvent,
  SafeAreaView,
  TextInput,
  TextInputChangeEventData,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import { ChangeEmailState } from '../../../../store/types/child/interface';
import {profileSettingsStyle} from './assets/settings';


const ChangeEmailComponent: React.FC = () => {
  const [state, setState] = React.useState<ChangeEmailState>({
    oldEmail: '',
    newEmail: '',
    password: '',
  });

  const onChangeOldEmail = (
    args: NativeSyntheticEvent<TextInputChangeEventData>,
  ) => {
    setState({
      ...state,
      oldEmail: args.nativeEvent.text,
    });
  };

  const onChangeNewEmail = (
    args: NativeSyntheticEvent<TextInputChangeEventData>,
  ) => {
    setState({
      ...state,
      newEmail: args.nativeEvent.text,
    });
  };

  const onChangePassword = (
    args: NativeSyntheticEvent<TextInputChangeEventData>,
  ) => {
    setState({
      ...state,
      password: args.nativeEvent.text,
    });
  };

  const onPress = () => {
    console.log(state);
  };
  return (
    <SafeAreaView style={{flex: 1}}>
      <KeyboardAwareScrollView>
        <View style={profileSettingsStyle.root}>
          <Text style={profileSettingsStyle.title}>Change Email</Text>
          <View style={profileSettingsStyle.ctxInput}>
            <TextInput
              placeholder="Old Email"
              value={state.oldEmail}
              onChange={onChangeOldEmail}
            />
          </View>
          <View style={profileSettingsStyle.ctxInput}>
            <TextInput
              placeholder="Old Email"
              value={state.newEmail}
              onChange={onChangeNewEmail}
            />
          </View>
          <View style={profileSettingsStyle.ctxInput}>
            <TextInput
              placeholder="Password"
              secureTextEntry={true}
              value={state.password}
              onChange={onChangePassword}
            />
          </View>
          <Button style={profileSettingsStyle.button} onPress={onPress}>
            <Text style={profileSettingsStyle.textButton}>Save</Text>
          </Button>
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

export default ChangeEmailComponent;
