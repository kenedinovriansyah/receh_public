interface GenderProp {
  label: string;
  value: string;
}

export const ChoiceGender: GenderProp[] = [
  {
    label: 'Male',
    value: 'Male',
  },
  {
    label: 'Female',
    value: 'Female',
  },
];
