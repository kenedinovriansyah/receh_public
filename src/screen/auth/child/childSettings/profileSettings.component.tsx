import {Button, Picker, Text, View} from 'native-base';
import React from 'react';
import {
  Image,
  NativeSyntheticEvent,
  PermissionsAndroid,
  Platform,
  SafeAreaView,
  TextInput,
  TextInputChangeEventData,
  TouchableOpacity,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {profileSettingsStyle} from './assets/settings';
import DatePicker from 'react-native-datepicker';
import {ChoiceGender} from './child/choiceGender';
import _ from 'lodash';
import * as imagePicker from 'react-native-image-picker/src';

interface ProfileSettingsState {
  first_name: string;
  last_name: string;
  avatar: any;
  date: Date;
  gender: string;
}

const ProfileSettingsComponent: React.FC = () => {
  const [state, setState] = React.useState<ProfileSettingsState>({
    first_name: '',
    last_name: '',
    avatar: '',
    date: new Date(),
    gender: 'Male',
  });

  const onChangeFirstName = (
    args: NativeSyntheticEvent<TextInputChangeEventData>,
  ) => {
    setState({
      ...state,
      first_name: args.nativeEvent.text,
    });
  };

  const onChangeLastName = (
    args: NativeSyntheticEvent<TextInputChangeEventData>,
  ) => {
    setState({
      ...state,
      last_name: args.nativeEvent.text,
    });
  };

  const PermissionCamera = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera Permissions',
            message: 'Apps need camera permissions',
            buttonPositive: '',
          },
        );
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.log(err);
        return false;
      }
    } else return true;
  };

  const PermissionExtenal = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Write extenal storage permissions',
            message: 'Apps need write extenal storage permission',
            buttonPositive: '',
          },
        );
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.log(err);
        return false;
      }
    } else return true;
  };

  const onLibraryImage = () => {
    imagePicker.launchImageLibrary(
      {
        mediaType: 'photo',
        maxWidth: 120,
        maxHeight: 120,
        quality: 1,
      },
      (res) => {
        if (res.didCancel) {
          console.log(res);
        } else if (res.errorCode === 'camera_unavailable') {
          console.log('Camera unavailabel');
        } else if (res.errorCode === 'permission') {
          console.log('Permissions');
        } else if (res.errorCode === 'other') {
          console.log('Other');
        }
        if (res !== state.avatar) {
          setState({
            ...state,
            avatar: res,
          });
        }
      },
    );
  };

  const onCamera = async () => {
    const PermissionC = await PermissionCamera();
    const PermissionEx = await PermissionExtenal();
    if (PermissionC && PermissionEx) {
      imagePicker.launchCamera(
        {
          mediaType: 'photo',
          maxHeight: 120,
          maxWidth: 120,
          saveToPhotos: true,
          quality: 1,
        },
        (res) => {
          if (res.didCancel) {
            onLibraryImage();
          } else if (res.errorCode === 'camera_unavailable') {
            console.log('camera_unavailable');
          } else if (res.errorCode === 'other') {
            console.log('Other');
          } else if (res.errorCode === 'permission') {
            console.log('Permission');
          }
          if (res !== state.avatar) {
            setState({
              ...state,
              avatar: res,
            });
          }
        },
      );
    }
  };

  const onChange = (args: any) => {
    setState({
      ...state,
      date: args,
    });
  };

  const onChangeGender = (args: any) => {
    setState({
      ...state,
      gender: args,
    });
  };

  const onPress = () => {
    console.log(state);
  };
  return (
    <SafeAreaView style={{flex: 1}}>
      <KeyboardAwareScrollView>
        <View style={profileSettingsStyle.ctxAvatar}>
          <TouchableOpacity onPress={onCamera}>
            <Image
              source={{
                uri: state.avatar
                  ? state.avatar.uri
                  : 'https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/plus-512.png',
              }}
              style={profileSettingsStyle.avatar}
            />
          </TouchableOpacity>
        </View>
        <View style={profileSettingsStyle.ctxInput}>
          <TextInput
            placeholder="First Name"
            value={state.first_name}
            onChange={onChangeFirstName}
          />
        </View>
        <View style={profileSettingsStyle.ctxInput}>
          <TextInput
            placeholder="Last name"
            value={state.last_name}
            onChange={onChangeLastName}
          />
        </View>
        <View style={profileSettingsStyle.ctxInput}>
          <Picker selectedValue={state.gender} onValueChange={onChangeGender}>
            {_.map(ChoiceGender, (base, index) => (
              <Picker.Item
                value={base.value}
                label={base.label}
                key={index}></Picker.Item>
            ))}
          </Picker>
        </View>
        <View style={profileSettingsStyle.ctxInput}>
          <DatePicker
            style={{width: 200}}
            date={state.date}
            mode="date"
            placeholder="select date"
            format="YYYY-MM-DD"
            maxDate="2005-05-01"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: {
                position: 'absolute',
                left: 0,
                top: 4,
                marginLeft: 0,
              },
              dateInput: {
                marginLeft: 36,
                borderColor: 'transparent',
              },
            }}
            onDateChange={onChange}
          />
        </View>
        <View style={profileSettingsStyle.ctxInput}>
          <TextInput
            multiline={true}
            numberOfLines={4}
            placeholder="Bio"
            style={profileSettingsStyle.textarea}
          />
        </View>
        <Button style={profileSettingsStyle.button} onPress={onPress}>
          <Text style={profileSettingsStyle.textButton}>Save</Text>
        </Button>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

export default ProfileSettingsComponent;
