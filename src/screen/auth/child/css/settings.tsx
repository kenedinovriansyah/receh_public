import {Dimensions, StyleSheet} from 'react-native';

export const settingsStyle = StyleSheet.create({
  title: {
    marginTop: 10,
    fontSize: 16,
    marginBottom: 10,
    marginLeft: 6,
    textTransform: 'uppercase',
    fontWeight: '700',
    color: '#8a8b90',
  },
  button: {
    width: Dimensions.get('window').width,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: 'transparent',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginBottom: 10,
    elevation: 0,
  },
  buttonText: {
    fontSize: 17,
    color: 'black',
    fontWeight: 'bold',
    textTransform: 'capitalize',
    width: 200,
  },
  buttonIcon: {
    fontSize: 20,
    color: '#00b09b',
  },
  ctxIcon: {
    backgroundColor: 'white',
    width: 40,
    height: 40,
    elevation: 10,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  buttonIconRight: {
    fontSize: 20,
    color: '#8a8b90',
    width: 30,
  },
  divider: {
    borderWidth: 0.6,
    borderColor: '#00b09b',
    marginRight: 8,
    marginLeft: 8,
  },
});

export const generalButton = [
  {
    name: 'Profile',
    router: 'settings',
    icon: 'user-circle',
    right: 'chevron-right',
    routerTarget: 'profile',
  },
  {
    name: 'Change email',
    router: 'settings',
    icon: 'user-cog',
    right: 'chevron-right',
    routerTarget: 'change_email',
  },
  {
    name: 'Change password',
    router: 'settings',
    icon: 'key',
    right: 'chevron-right',
    routerTarget: 'change_password',
  },
  {
    name: 'Language',
    router: 'settings',
    icon: 'language',
    right: 'chevron-right',
    routerTarget: 'change_email',
  },
];

export const supportButton = [
  {
    name: 'Report a problem',
    router: 'settings',
    icon: 'pencil-alt',
    right: 'chevron-right',
    routerTarget: 'change_email',
  },
  {
    name: 'Help Center',
    router: 'settings',
    icon: 'question-circle',
    right: 'chevron-right',
    routerTarget: 'change_email',
  },
  {
    name: 'Safety Center',
    router: 'settings',
    icon: 'shield-alt',
    right: 'chevron-right',
    routerTarget: 'change_email',
  },
];

export const aboutButton = [
  {
    name: 'Community Guidelines',
    router: 'settings',
    icon: 'shield-alt',
    right: 'chevron-right',
    routerTarget: 'change_email',
  },
  {
    name: 'Terms of use',
    router: 'settings',
    icon: 'book',
    right: 'chevron-right',
    routerTarget: 'change_email',
  },
  {
    name: 'Privacy Policy',
    router: 'settings',
    icon: 'file-alt',
    right: 'chevron-right',
    routerTarget: 'change_email',
  },
  {
    name: 'Copyright Policy',
    router: 'settings',
    icon: 'copyright',
    right: 'chevron-right',
    routerTarget: 'change_email',
  },
];
