import {Button, Text, View} from 'native-base';
import React from 'react';
import {FlatList, Image, ImageBackground} from 'react-native';
import {imageArray, profileStyle} from './css/profile';
import FontAwesome from 'react-native-vector-icons/FontAwesome5';
import _ from 'lodash';
import {StackNavigationProp} from '@react-navigation/stack';
import {ParamsListProps, RoutePath} from '../../../routes';

type RootProp = StackNavigationProp<ParamsListProps>;

interface NavigationProp {
  navigation: RootProp;
}

interface FlatListProp {
  item: any;
  index: number;
}

const ProfileComponent: React.FC<NavigationProp> = (
  props: React.PropsWithChildren<NavigationProp>,
) => {
  const onRouter = (args: RoutePath) => {
    props.navigation.navigate(args, {
      name: 'default',
    });
  };
  const contentImage = (base: FlatListProp) => {
    return (
      <Image
        source={{uri: base.item}}
        key={base.index}
        style={profileStyle.listImage}
      />
    );
  };
  return (
    <View style={profileStyle.root}>
      <ImageBackground
        source={{
          uri:
            'https://static.wikia.nocookie.net/doraemon/images/d/d9/NobitaNobi2005%28Original%29.png/revision/latest?cb=20201020234421&path-prefix=en',
        }}
        style={profileStyle.backgroundImage}
        resizeMode="stretch"
        imageStyle={{
          borderBottomRightRadius: 35,
          borderBottomLeftRadius: 35,
        }}>
        <Image
          source={{
            uri:
              'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxIQEBUSEhMWFRUXGBcXFRgYGBgXFhgXFRUYGBgXFRUYHSggGB0lGxcVITEhJSorLjAuGB8zODMtNygtLisBCgoKDg0OGxAQGy0lHyYtLS0tLSsrLS0tLS0tLS0tLS0tLS0tLS0tLS0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABgEEBQcIAwL/xABGEAACAQMCAwQHBQQIBAcBAAABAgMABBESIQUxQQYTUWEHIjJxgZGhFCNCUmKCkrHBM1NjcnOTotEWQ4OjFyQ0VHSy4RX/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAgMEBQEG/8QAKBEAAgIBBAEEAgIDAAAAAAAAAAECAxEEEiExQQUTIlEyYVKBFDNC/9oADAMBAAIRAxEAPwDRtKUoBSlKAUpSgFVpV9ZcNaTyHif5UBZAVcw2EjclOPPapBa8PSPkMnxNXWarc/o9MAvBH6lRVW4G35hWdqteb2CNTcJlXpn3VYshGxGKmZrwubVJBhh/vXqn9nhEiKpWUu7BkyOa8wfA+FY9oSBnY+4g/wAKsB50pSgFXfDr+S3kWWJisiEMrDoRVpVaA6p4PeLxGwSRl9W4iw69PWBVgPjn6VH+wvZmHhcrwlczOGaOc7iSJTui/kZQV1L1zkEjOMx2Ai0cLtB/YqfmSf51e8aQn7OwGStxEdhnAOpXz5aWbNcvdiTh4NyXCZadouytvfyQvcBmEOsqgbCNr051jGT7I5EVgvSd2tPDLdIrcBZZQQhxtEiYBZV5Z5ADkKnVYzjfDrSVe8u44mWMFtUoGEHU5PIVGufySlyiU48ZRzVw/g95fyHuYpJ2JJZgM7nclnO3zNXHaPsfecPVHuYwquSFIYMNQGdJI5HG9bT4v6W7O2HdWcJlxyIAii28BjJ+QrV3aztdc8SYGdhpX2I1GETPh1J8zXRjKbfWEY5KKI5SlKtKxSlKAUpSgFKUoBVQKVk+D2WttTeyPqaN4B68L4XnDvy6D+ZrOCigkgAEkkKoHMk8gAOdSvsx2Ie4X7RcusNuNxnSS4HMkt6oTzOQemRuaJS+ySjkwXCOC3F4cW8TOBzb2Yx75G2+AyamXDvRe53uLkL4pEmo/wCY5x/pqW8LlVR/5ZLu5QAKMKFhGORjMgjTl1Xar+Liya1jlSSCRvZWVNIY77JIMox2Pqhs9cVnslYulwXwjDyzBQejrh6+0kjn9UrgfJMCsVxHszZ960XdpEBsqQpLdXL5A9dgCRAuc8wfEkcqmfFJnPdwwnEsz92rYz3ahS0kuDsdKjb9TLUi4RwqK1jEcS4HNid3durSOd3Y9SalRGUuWzyxxjwjS3FOwUaLqR72LzltjLH+13XrIPMjAqP3HZudMadMobOgxkESY5iJuTsMElNnAHs10sBUT7WdmlZHmhjBc4aWIeqLgLjB29mdcZSUbggDly0uBTnJz6QDsR7wR16gg8j5VguLcPCesvLr5VuLtdwNJbA3wYPIulhKo0tPAxUA3C4H3oB3bn6vnitcTx6lKnqKhCXIlHBDzVK+5FwSPCvkVeRFV019xRFiAASTsANySeQA61vH0d+jZLcLc3ihptikRwVi8C45M/0HmahZYoLLJwg5PglvYKN14ZaLIpVhEoIPMbnGR02xWepXzLIFUsxAABJJ2AAGSSegrkye6WUb0sI+qsOO8JjvbaS3lz3cgAbBwcqwZSD5MoPwqGcf9LVlApFvquJNwMKyRg+JZhkj3DetP8e7WXd5KZJZnz0VWZUUeCoDgfx860Vaefb4KbLYrhG05vQpbEerdTA/qRGH0xn51qDtBwz7LcyQd4sndsV1r7LY6j54+FeT8XuCNJnlI8DI+PlmrMtmt0IyXbyZpNPpHzSlKmQFKUoBSlKAUpSgPuNCxwOZqVwosUYHRRv/ADqKRSFSCOYqR8LujKh1YyNqhPOAbT7M9klTuVk9aa4QySkcorYacxK2NmkLIrHnp1gcqm/BuHLeuZ5ADbxvptoyPUZom0tO6nZvWUhOgC5GcjGD4BPp4cb3UCfsagfp+yxyBhn/ABNRqf8AAbUQ20MQ/BHGvyUA/Wqq/lJt+C6fCSRfBatuI8PjuI2ilQOjDBU/x8iDuCORq7q24hfxW8ZkmkWNBzZ2CqM+JNaCkhvZtXHE2gmZme2t2VXbH3kc8qFJCR+LTFpPLdWPWp2KjM7qOI2sysCk8E0IIIIZlMc0ZB5EaRN+9WH4P6ToLjiTWHdsnrukchZSHePII081Bw2PdXiSXR63kn1UNVqhr08IDdcPyt/ZfhOt4x4Jcx6sfCUS494rRsL6lDeIB+YzW+eJ3rR8VlYQyyr9ntVYxhDoYS3TesrMCfVdfZBrR81uRcNBGjk946ogRteAx0jRjI2xtWXGJMtlzFEO4kuJXHnXgq1kON2MsU7rLG0bZ9lgVOCNjg1sH0Z+jo3BW7u1Ih2aOM7GUj8TeCZ+fuq+U1GOWQjFt4M76JOxAhRL6dcyuAYFP/LUjaQj8xB28AfOtoUpXKssc3lm+EFFCtQ+mLtnz4fA3/yGHzEQP1b5VIfSd25FhH3EDA3Ljnz7pT+M/qPQfGufpZCxJO5O5J5knqa1aaj/AKkUXW+EUJqlUpW4ylapSlAKUpQClKUApSlAKUpQCslwSfTJg8m2rG19K2DtQG/fRrMLjh9zZnmveAD9FwrHPu1l/nW0ezl131pDJ+aJCfI6RqHvByPhXPHo67RrbXUcjnCODFLzwASCrEDwbT+8a3D2Y4q1u7RzRmGCaRntix3VnOWjlGPui7EuoJ/EV2IwaIfGTX2Wv5RJzWtvTrZPJYwyAMY4p0ecLzEZDKWxy9UkHflzrY4aviZVYEMAQRgg7gg8wR4VeVGl+DXjCwtntO8kig4m5tA4+8eBLWZ3jHn/AEqj3AVd8M7LXbcRgeLu24eLlr6OZSuo96uShOc8zjGMcznpWZ4reC4vbaK1YQ28XffeoqBXmCAGODUNJwpbLgHfUBuDXpwW3u7SWdIJRJGCrhJk0ITKCzBJIx92dQycKR6/IHOanak8MsUG1wbErwvbtIY2kkYKigszE4AA5k1HTxu+wMWkAPUm6YqPPaDLe7A+FWzWMs7LJdyCXS2pIkUpApHIlSSZWHQtsCAQoNeSvikFVJsrwcMwkncFXnfvdLe0iaQkSEdCEUEjxdqgN24suOXExBYCCS5Cjm2qJF0jzLjHvNbOqJ9oLOIX32mRioitNRwMlhFdxyafedIX9qsdc8zefJplDEVg8uOcEgvTw+a40TESAFkyEcPG7gc8lNaDb38t6mCjHL4fConPK1ullGY2cxK1xcBdyiKjKzAfiIeYnSNyEbGSMVKopFdQykFSAQRuCCMgg+BBqFucfolXg+6h/pF7aLwyEKmDcSD7tTuEXl3jjwzsB1PuNSPjHFIrSB55m0ogyT4nkFHiScAe+uY+0/HJL+6e4l5sdh0RR7KDyA/nU9NTueX0Rus2rCMfd3kkztJIxd2JLMdySepNW9VqldIxClKUApSlAKUpQClKUApSlAKUpQCvoV819UBurivYm1i4ekUAU3qosx3++kGklwo9xYqB+Sp/wOZbywheVVcSwprVhlSSMNkHnuDWFcxT2trcliIpe471gd0ZY2jikBx6hSYrv5AnYVleyCOluYpSDJFLMjkbA/es6kAcsqynHnXPteY5/ZsrXJdwWUsQ0w3UyL0R9MyjyUyAsB4DUcVS44Y021xcTTLz0ZEcR8mSIDWPJiR5V4Xk0sl19nSUwhYhLkKrPIS5XC6wQFUL622fXXcVf39/FbprmcIuwy3U+CjmxPgKg52YXJJQhno9PsseFXQuEIKDSMKV9kqMeqR0xVvxPisNsuqaQLnkNyzeSIMs3wFRninaiV1PdD7PGB60sgHeafFIzsnTd8n9NR+GOSRi6ZUnnNLl5nH6QfZHvwP015Gtv8jVChskV52mnl2gQQr+eQB5CP0xKcL+0SfKsTNCZTmaSWUnq8jY/ZRSFX4AV5jhw/FLMx8dePooA+leMsMyssUbmQzHu01YDozKTqJGA6gAk7ZGOtWbGujX7UK45aJ52VmZ7KBmYsSg9YnJYAkKxPUkAHNWfFLyBLw99g6YFwmNbO7T6kCR7l2zFkbbHfaszbwx28SoCFjiQLk7BUReZ8MAZqx4BFr13TLhpzqQEYZIQoWNPEZA1keLnwrNOxVpyZzJc8I9eD2z5eeYYllxlcg93GmQkQbqRlmY/mY+Aq34cRbTS25wsWnv4STsqFj3yeSo5DDwEgHSs3WrfTpfiOG3RX0yMZAwB3MLBdQbH4Sypt101m0tkrbsPpkZ/FZIZ6UO2x4hN3UJItoydPMd4wOO8Pl4A8h76gWaqxr5r6CMVFYRilJt5YpSlSPBSlKAUpSgFKUoBSlKAUpSgFKUoBVapVaA2n6K+2sUEbWF5juW1aGbdB3ntpJn8DZO/j762fbKlpMuk5t7gIqtq1BZkGlMyE794mlQSecYHWuXga2V2G9H13ewLJJO9vbE6kUFiz4OQyx5AAyNm+IrLfGEVuk8Ivrm+jdd/wAPinAEi6tJypyVZDjGUdSGU422NRfjHZdo5PtFupk2wyMzPKAOsEkhJ36oTjqD0OXtYbu1VVJN5GBjUdKXIx5exL81Pvr2tu0Vq5wZVjbJXRL90+pScqA+NRBBG2eVYoS/jyjTGWGn0yB27LPM2c4i04jYFW7wjJZkYZ22Az11HwrJ1Mb3hUFyMyRrJjk34h/ddcMPgaxv/B8HIPcAeAmY/Vst9avjZFI3Q1eO0RyedUxk7tsqjdmPgijdj5CpB2Z4M6N9omGJCNKR8+6Q88nkZG21Y2GAB1J947WysPW9VHbYFmaWdz+VdRaRj+lflV3Dw+e9P3itb23VCdNxMDnZ8f0CctgdZ5HTyMvlZwujNqNW5cFg8y38zRKQbeEjvvCZ9ysQ8YxjLEcyAvLNSGsbwxFE93oUKizLEgAwoWK3i9kDoGZx8K++O8Wjs7eS4lOEjGfMnoo8ydq5GrTld7cSmD+O5mL7b9rYuGW+tvWlYERR9WI6nwQdT8BXN/G+LS3kzTzMWdjufAdFA6AdBVz2n49Lf3Dzyndjsv4UUeyijoAPmaw9drSaWNEf2ZLJuTKUpStZWKUpQClKUApSlAKUpQClKUApSlAKUpQClKrQF/wKy+0XMEP9ZLHH++4X+ddYRRKihVGFUBVHQBRgD5CuWex84j4haOeS3EJPuEi5rqk1xvVm8xXg1addspVv2eCmS6t5FVsSCZFIBHdzqN8H+1Sb6VacE9VriP8ALO7fCYLKP9TuPhVeJSm3ljux7KApcADJMDb6gBzMbAN19XXjnWfRTVV219Mlat0cmQ4j2MspUZViEJYY1wfdOvmpTG9Q3idgLDC3EbTliRE0dzcKG/VLbl2MSrtqdA464GcVOeOcdEIRIgJZpQTEgOxXrLIwzpiXIy3mAMkgVjeG2BjLSSP3s747yXGM45JGufu4x0Ue85JJrp6vUV1LnspqUm+CvYrgFvCpuUMUksoGZI90CjOI4jknAzuScsdz4CUSuEUsxwACT5ADJ+lRObhShzLAzQSnm0eNLnb+mi9mTljJww6MK8eJXt3PF9kkh0mTCyTxnMJh5yYBOtHZRpCnONeQSAa8q1tU456PJVSTPTs4SbdZGBDTFpyDzHfuXAPmFZR8KjnpkA//AI8vk8WP3x/+1NQPl0/2qDeml8cIceMsQ+pP8q5FE9+qUv2aZLFeDncmqUpX0xhFKUoBSlKAUpSgFKUoBSlKAUpSgFKUoBSlKAVWqUoD3tCda456lx787V10nIZ54Ga5J4ZOI5o5GGQjoxHiFYEj5Cup+Ecdt7xQ8MinIB0k4dQwyNSHcVyfVYyko4Ro07xk8m+7vgc7TxafLvLcswx5mORv8ustisV2lQCAuWVHiIliZyFXWn4ST0ZdSHyc1ecNvUuIUmjOUdQy+OCOR8xyPnXKsjJwjP8Ao0J84PLhnCYbbV3KaNWM7k4A9lFz7KDJwo2GTV9SlUSnKTzJk0khVMVWlRPRUC9Nqk8JbHSaIn3esP4kVPajnpDse/4XdJjJEZcDzjIcfwNadJJRui39kLF8WcwGqVU1SvqznClKUApSlAKUpQClKUApSlAKUpQClKUApSlAKUpQH0DXS/Y2xhn4VZ97GkmIEA1KGIxnkTuK5nFdLei2Utwi1J6K6/uyMP5Vl1f4F+nXyM3BwS2RgywRhhyOkEj3FskVi+yt/quuIQD2YrjK+XeorOB/1A7ftGpHUH9G8he54q563ZH7uoc65tnNMsmmXDROqUpXHLBSlKAV8SIGBVuRBB9xGD9M191QmpRymsHj6OT+0HDGtbqaBucbsvwB2PyxWOqW+lDiMVzxOaWHdPUXUPxFEVC3zGPhmsDwng891II4I2kc9FGcebHko8zX18JZgmznNc4RYUxW0uC+hyVzi5uo4mABaOMCWQBs41esAucHfflWcm9CluV9S7lVuhZEZfioIP1qLvgnhskqpPwaQpWe7W9mJuGz9zMAcjUjj2XXxHhvsR0rA1YnlZRW1gUpSvQKUpQClKUApSlAKUpQClKUApSlAVrqLsFa9zwu0T+yVj739c//AGrl2useAgC0tx/YxD/trWTWfii/T9l/Wv8AsHexpxTiltkBmn7xB+bmG0jqRkVPTOgOkuoPUFgCPeK51WcXHE7qboZJGU77euQCCORxjeqKNO7lKH2T1Fygt30dHZqtaftO099EMLcsw8JAsgHxYavrV1/xxxD88P8Ak7/V6ol6FqE+MFEfU6cG1qoxwMnYdSdh8ztWorjtVfPzuWX/AA0jT66SfrWIvHaY5mZ5f8Rmf5BiQKtq9Atf5vBXP1WtfijanE+2llBkCXvX/JENfzceovxaoH2l7XT3alD9zAdiikl3/TI45g/lUYPXNYRmCjJ5CljZvPLGij7yRgiZ3CaskkjrhQWPuxXTp9Ko063PloyS1tlzwuDw4D2Fk4ldM2THApBkbAyOXqKPzkb4/D16Cts21jFZqtlYIsbsuqR8ajGnIyyt+NychQeZyeSmslBDDYWmFGIoVZj1ZsZZm/U7N8yarwS0ZELyf00p7yXyP4Yx+lFwo9xPU1htuc2348HWqr2pLye/D+HxwLpQbnd2Jy7t1aRzuzef8KuqUrG232aUsGuPTpZq3D4pTgMk6hT1xIj6h/pB+FaGNbh9PPFwTb2g6ffP5Egog+Wo/GtPGupp01WsmG1/IpSlKvKhSlKAUpSgFKUoBSlKAUpSgK1NOxXo8ueIjvMiKDOO8bOWxzEaj2vfsKwHZfhX2y8htuXeOFJ6hebEfAGupra3SJFjjUKiAKoHIKOQrPqLnWsIuqr3dmu09F/CbVQbqZjnrJKsSnxwBj+NSSzisWAjhvWIAChUvCcLjAA9Y9Kz13JGimSTSFQElmAOB5e/bYczirGPhc96u6JbQn2dcSSXLA/iMbgpD7mDHfcA7VnhvtLpbYET4h6L4SC0Eh1HJAnVZFJJ3zIqh999zqPvqHz27wyNFIndumNS7cjyZSPaU42PI4+FbjtexCW6n7NcTxud8sweInwMGAgB/Rpx0xUY7aWZmj0zxiO7jDNAyk93cKu7xxucblQW7tvWBXIyN636e2dUueUYNTTC2LceGQClUVgQCNwdx8arXaXJwWsClKV6DzkTJHgDk+Z6f71KPR1ba7/UeUcLkf3neNQf3dfzqN1KPRpOFvXT88DEe+OVPrh/oax6/wD0yZt0LzdFE34xardSw2ZJCuWlk0nBCQYxgjke9eEj+7SS+ktDovfVXktyB9y45DvD/wAmTlkH1STsegv+BDXfXDc+7jhiHkX1yN9DF9KkpjBGCPfXHhTGVaTO3KxqeUR9GBAI3B5Ebg+4ivO7uUijaWQ4RFLOfBVGTVzcdkbNjqEZjOcnuZJYMnxIhdQfjWrfTisFlbxwRvM00xOrXcTOBEnP7tn0btgZI6Gq1o+eyf8AkcGou0vF2vbqW5bYyMTjwUbKvwUAViarmqVuSwsGVvIpSlAKUpQClKUApSlAKUpQClKUBNPRAyjjFvq/tQPf3TYro2uUOAcTa0uYrhNzG4bHiBzHxGR8a6k4XxGK6hWaFg0bjKnw8QR0I5EVg1kXlM1aeXGD4tbb7Te4beO2COV5hp3yUJ/uINQHi4PMCpcFqM9k9ri+B5meJ/erWkCqfmjj9k1KK1UrEFgoseZMpira/wCHxToY5UV0PNWGQfA+R86uqVaQNQ9oPRnND61ke9j/AKpziRQAfYkOzjls2D5moTcxtE2mVHjYHBV0ZTnwAI9b4ZzW/O0PE2hVFiUNNK2iJTnTnSWZ3xyRVBY+OABuRUf4p2VWeF+8lkluMao5XdsJKu6GOJSERQwX1QN8bk860V3zisGO7T1y58mtbTs5dyDUIggP9Y+g48dIBI+IFfPEuz9zbprZEdRjUY2JKgn2irKNvE5251sGxuO9iSTGNaqxHgWAJHzOPhXtWjfLvJz8pPGDU8iFWKspVgcMrDBHhnyI3BGxHLNXnBOIC2uopzsqtiQ/2bjS/wAgQ37NZrtJwn7qXu0y9tH3sOB7doSQ8Hie6f1l8FcKKgnZ26k4jdxWkaBRK2HbOSsfN2z0wufpUJ3RlBxmaq6JKanDo6I7FRZga4PtXEjzfsE6If8AtJH7s1IajdzxRlf7JZIpaNVDu/8AQwAqNCsF3kcjB0KRtuSuRmi294Rk3p1fpgiCfBWy2P2vjWJR+joyms8klY1yT6Tu0H2/ic0oOY1PdRf4cZIB+JLH41vXtjxy+tOH3JkRZvu2VJoQy6C/qgywsSVAB9pWIzzArmBqBPJ80pSh6KUpQClKUApSlAKUpQClKUApSlAVrPdmu1l1w9y1vJgH2kbeNv7y55+YwawFVrxpPhnqeDc/BPS/ELiOaaFoyR3VxoOpGTVqWQA+tqRi22+Q7DcgVt3g3bGwu/6C6icnHq6gr7+Ktg1x3SiSSwg3k7hzVa454R2uvrTAgupowNgoYlRjwVsjHwqacK9OHEYtplinHmuhv3k2+lenhu67XVxIH+rtTp8PvpgGPyhUZ/3q+ZgASeQGT7hua1Vwj0tW11fQPJG1vlHhkLMrRAMyujltiMMpB25OalvG+MpdRtb2r94Hys0qHMaRnZ1Rxs0jDUo0505JOMbziUW8PLLLgX/pYfNA3wf1h9CKvqAYGByGw8gOlK1Lo5EnltnipxeWhIBV3khkz1SWBzj4vHHUN7E9iYbcTGZFlbvpY01b4jjcpuOW5Bz8KmWCbqzUdbjJ9yQTPn5gD4ivmybJlYcjPcEfCZx/EGqsJzNam1QedpbyWpJtCiKTloWB7gnqV0+tEx8RkdSprKR9pXG0tpKD+aMxyof9Sv8A6at6VJ1plUdRJdntc9oy3qpaTPnI+87uOPBG+ss5bBHgprSXaz0czwAyw4lTcsqA5jyc4AO7KOQPPyrc5qoNee0ia1ckzloiqGtt+lHsgndtewKFZd5lGwYHbWB0YHGffmtSGqJRcXg6NdinHKKUpSolgpSlAKUpQClKUApSlAKUpQClKUApSlAKUpQHtbysjBlOCCCD4EHauieynHEvrVJV2PsyKPwuOYwOQPMeRrnGs92V7TTcPl1xnKnAdD7LjPXwPgelThLayi+p2R47Oh6rUa4D25srsD7wRP1SQ6d/0sdmrKcS4skSjS0bO5IjBdQpIGSXbkqKPWJ8BtknFad6xk5ftTzjB7RcQSCS4vHP3dlCw5855cNo94QRj/q1h+zHFdVuuhlulUAvJb5eQFt2M1rjvEOoscqGHXblUC9InbCOSFOH2jl4lYyTzbj7ROSSxC/kycj4dAM6/trt4mDxsyMOTKxVh7iN6zb2pZR1PYi4KLOmLW+ilz3ciMRzAYagfBl5qfeKuCK0Lb+kK9wBN3VyBjHfxK7D3SjDj96sva+klV52rg9e7vJ1X4JJrVR7hViu+zNLRfTNx0xWqP8AxRi/9vde77YuPn9mzVleeklGzosst0NxdTzp7+69RD+7XvvIitFLyybekHj8EVlNEGDSSKYwF9YKWG+sjZdt8c/KtDtzrKcb49NduGlYYXZEVVSNB4JGowv8axZqmUtzNtVSrjhFKUpUS0UpSgFKUoBSlKAUpSgFKUoBSlKAUpSgFKUoBX2tKUJR7KnlXt+D4fzFKVJdFUuzxbnXxSlRJlKrSlAVFUpSgKmvmlKAUpSgFKUoBSlKAUpSgP/Z',
          }}
          style={profileStyle.avatar}
        />
      </ImageBackground>
      <View style={profileStyle.groupButton}>
        <Button
          style={{
            ...profileStyle.buttonAuth,
            width: 260,
            backgroundColor: 'red',
          }}>
          <FontAwesome name="heart" size={25} color="white" />
          <Text
            style={{
              color: 'white',
              fontWeight: 'bold',
              textTransform: 'capitalize',
              fontSize: 17,
            }}>
            Interest with you
          </Text>
        </Button>
        <Button
          style={profileStyle.buttonAuth}
          onPress={onRouter.bind('', 'settings')}>
          <FontAwesome name="cog" size={25} />
        </Button>
      </View>
      <FlatList
        contentContainerStyle={profileStyle.flatlit}
        data={imageArray}
        renderItem={contentImage}
        keyExtractor={(item) => item}
      />
    </View>
  );
};

export default ProfileComponent;
