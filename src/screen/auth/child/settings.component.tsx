import {Text, View} from 'native-base';
import React from 'react';
import {Button} from 'native-base';
import {
  aboutButton,
  generalButton,
  settingsStyle,
  supportButton,
} from './css/settings';
import {FlatList, SafeAreaView, ScrollView} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome5';
import {StackNavigationProp} from '@react-navigation/stack';
import {ParamsListProps, RoutePath} from '../../../routes';
import { RouteListPathProp } from '../../../routes/child/childSettings.router';

type RootProp = StackNavigationProp<ParamsListProps>;

interface NavigationProp {
  navigation: RootProp;
}

interface GeneralButton {
  name: string;
  icon: string;
  right: string;
  router: string;
  routerTarget: string;
}

interface FlatListProp {
  index: number;
  item: GeneralButton;
}

const SettingsComponent: React.FC<NavigationProp> = (
  props: React.PropsWithChildren<NavigationProp>,
) => {
  const onRouter = (args: RoutePath, kwrags: RouteListPathProp | any) => {
    props.navigation.navigate(args, {
      screen: kwrags,
    });
  };
  function generalArray(args: FlatListProp) {
    return (
      <Button
        style={settingsStyle.button}
        key={args.index}
        onPress={onRouter.bind(args.item, 'settings', args.item.routerTarget)}>
        <View style={settingsStyle.ctxIcon}>
          <FontAwesome style={settingsStyle.buttonIcon} name={args.item.icon} />
        </View>
        <Text style={settingsStyle.buttonText}>{args.item.name}</Text>
        <FontAwesome
          style={settingsStyle.buttonIconRight}
          name={args.item.right}
        />
      </Button>
    );
  }
  function supportArray(args: FlatListProp) {
    return (
      <Button style={settingsStyle.button} key={args.index}>
        <View style={settingsStyle.ctxIcon}>
          <FontAwesome style={settingsStyle.buttonIcon} name={args.item.icon} />
        </View>
        <Text style={settingsStyle.buttonText}>{args.item.name}</Text>
        <FontAwesome
          style={settingsStyle.buttonIconRight}
          name={args.item.right}
        />
      </Button>
    );
  }
  function buttonArray(args: FlatListProp) {
    return (
      <Button style={settingsStyle.button} key={args.index}>
        <View style={settingsStyle.ctxIcon}>
          <FontAwesome style={settingsStyle.buttonIcon} name={args.item.icon} />
        </View>
        <Text style={settingsStyle.buttonText}>{args.item.name}</Text>
        <FontAwesome
          style={settingsStyle.buttonIconRight}
          name={args.item.right}
        />
      </Button>
    );
  }
  return (
    <SafeAreaView style={{flex: 1}}>
      <ScrollView>
        <View>
          <Text style={settingsStyle.title}>general</Text>
          <FlatList
            data={generalButton}
            renderItem={generalArray}
            keyExtractor={(item) => item.name}
          />
          <View style={settingsStyle.divider} />
          <Text style={settingsStyle.title}>support</Text>
        </View>
        <FlatList
          data={supportButton}
          renderItem={supportArray}
          keyExtractor={(item) => item.name}
        />
        <View style={settingsStyle.divider} />
        <Text style={settingsStyle.title}>About Graf</Text>
        <FlatList
          data={aboutButton}
          renderItem={buttonArray}
          keyExtractor={(item) => item.name}
        />
        <View style={settingsStyle.divider} />
        <Button
          style={{...settingsStyle.button, marginTop: 6, marginBottom: 80}}>
          <View style={settingsStyle.ctxIcon}>
            <FontAwesome style={settingsStyle.buttonIcon} name="sign-out-alt" />
          </View>
          <Text style={settingsStyle.buttonText}>Log out</Text>
          <FontAwesome
            style={settingsStyle.buttonIconRight}
            name="chevron-right"
          />
        </Button>
      </ScrollView>
    </SafeAreaView>
  );
};

export default SettingsComponent;
