import React from 'react';
import {User} from '../../../store/types/interface';

interface ContextProp {
  signIn: (args: User) => void;
  signOut: () => void;
}

export const AuthContext = React.createContext<ContextProp>({
  // tslint:disable-next-line:no-empty
  signIn: (args: User) => {},
  // tslint:disable-next-line:no-empty
  signOut: () => {},
});
