import {StackNavigationProp} from '@react-navigation/stack';
import {Button, Text, View} from 'native-base';
import React from 'react';
import FontAwesome from 'react-native-vector-icons/FontAwesome5';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import LinearGradient from 'react-native-linear-gradient';
import {ParamsListProps} from '../../routes';
import {authenticateStyles} from './authenticate';
import {
  NativeSyntheticEvent,
  TextInput,
  TextInputChangeEventData,
  TouchableOpacity,
} from 'react-native';
import {User} from '../../store/types/interface';

type ParamsProps = StackNavigationProp<ParamsListProps>;

interface NavigationProp {
  navigation: ParamsProps;
}

const CreateComponent: React.FC<NavigationProp> = (
  props: React.PropsWithChildren<NavigationProp>,
) => {
  const [state, setState] = React.useState<User>({});
  const onChange = (args: NativeSyntheticEvent<TextInputChangeEventData>) => {
    setState({
      ...state,
      email: args.nativeEvent.text,
    });
  };
  const onBack = () => {
    props.navigation.goBack();
  };
  return (
    <KeyboardAwareScrollView>
      <LinearGradient colors={['#00b09b', '#00b09b']}>
        <View style={authenticateStyles.disply_column}>
          <View style={authenticateStyles.displayTitle}>
            <Text style={authenticateStyles.childTitle}>Join to</Text>
            <Text style={authenticateStyles.titleB}>Gref</Text>
          </View>
          <View>
            <View style={authenticateStyles.field}>
              <FontAwesome
                name="at"
                size={25}
                style={authenticateStyles.iconInput}
              />
              <TextInput
                onChange={onChange}
                style={authenticateStyles.input}
                placeholder="email"
              />
            </View>
            <Button style={authenticateStyles.button}>
              <Text style={authenticateStyles.textButton}>Sign up</Text>
            </Button>
            <View style={authenticateStyles.group}>
              <TouchableOpacity onPress={onBack}>
                <Text style={authenticateStyles.textGroup}>
                  already exists accounts?
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <Text style={authenticateStyles.textAlign}>© 2021 GitHub, Inc.</Text>
        </View>
      </LinearGradient>
    </KeyboardAwareScrollView>
  );
};

export default CreateComponent;
