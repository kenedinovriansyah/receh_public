import {StackNavigationProp} from '@react-navigation/stack';
import {Button, Text, View} from 'native-base';
import React from 'react';
import {
  NativeSyntheticEvent,
  TextInputChangeEventData,
  TouchableOpacity,
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import LinearGradient from 'react-native-linear-gradient';
import {ParamsListProps} from '../../routes';
import {authenticateStyles} from './authenticate';
import FontAwesome from 'react-native-vector-icons/FontAwesome5';
import {User} from '../../store/types/interface';

type ParamsProps = StackNavigationProp<ParamsListProps>;

interface NavigationProp {
  navigation: ParamsProps;
}

const ForgotComponent: React.FC<NavigationProp> = (
  props: React.PropsWithChildren<NavigationProp>,
) => {
  const [state, setState] = React.useState<User>();
  const onChangeToken = (
    args: NativeSyntheticEvent<TextInputChangeEventData>,
  ) => {
    setState({
      ...state,
      token: args.nativeEvent.text,
    });
  };
  const onBack = () => {
    props.navigation.goBack();
  };
  return (
    <KeyboardAwareScrollView>
      <LinearGradient colors={['#00b09b', '#00b09b']}>
        <View style={authenticateStyles.disply_column}>
          <View
            style={{
              ...authenticateStyles.displayTitle,
              flexDirection: 'column',
            }}>
            <Text style={{...authenticateStyles.titleB, marginLeft: 0}}>
              Reset Password
            </Text>
            <Text
              style={{
                ...authenticateStyles.childTitle,
                textAlign: 'center',
                fontSize: 16,
                marginTop: 15,
                marginLeft: 20,
                marginRight: 20,
              }}>
              Enter your user account's verified email address and we will send
              you a password reset link.
            </Text>
          </View>
          <View>
            <View style={authenticateStyles.field}>
              <FontAwesome
                name="search"
                size={25}
                style={authenticateStyles.iconInput}
              />
              <TextInput
                onChange={onChangeToken}
                style={authenticateStyles.input}
                placeholder="Find username, email or phone number"
              />
            </View>
            <Button style={authenticateStyles.button}>
              <Text style={authenticateStyles.textButton}>Find</Text>
            </Button>
            <View style={authenticateStyles.group}>
              <TouchableOpacity onPress={onBack}>
                <Text style={authenticateStyles.textGroup}>
                  already exists accounts?
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <Text style={authenticateStyles.textAlign}>© 2021 GitHub, Inc.</Text>
        </View>
      </LinearGradient>
    </KeyboardAwareScrollView>
  );
};

export default ForgotComponent;
