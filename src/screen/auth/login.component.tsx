import {Button, Text, View} from 'native-base';
import React from 'react';
import {TextInput, TouchableOpacity} from 'react-native-gesture-handler';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import FontAweomse from 'react-native-vector-icons/FontAwesome5';
import {User} from '../../store/types/interface';
import {authenticateStyles} from './authenticate';
import LinearGradient from 'react-native-linear-gradient';
import {
  Image,
  NativeSyntheticEvent,
  TextInputChangeEventData,
} from 'react-native';
import googlePng from '../images/google.png';
import facebookPng from '../images/facebook.png';
import {StackNavigationProp} from '@react-navigation/stack';
import {ParamsListProps, RoutePath} from '../../routes';
import {AuthContext} from './context/ContextAuth';

type ParamsProp = StackNavigationProp<ParamsListProps>;

interface NavigationProp {
  navigation: ParamsProp;
}

const LoginComponent: React.FC<NavigationProp> = (
  props: React.PropsWithChildren<NavigationProp>,
) => {
  const [state, setState] = React.useState<User>({});
  const {signIn} = React.useContext(AuthContext);
  const onChangeUsername = (
    args: NativeSyntheticEvent<TextInputChangeEventData>,
  ) => {
    setState({
      ...state,
      username: args.nativeEvent.text,
    });
  };
  const onChangePassword = (
    args: NativeSyntheticEvent<TextInputChangeEventData>,
  ) => {
    setState({
      ...state,
      password: args.nativeEvent.text,
    });
  };
  const onSubmit = () => {
    const data = {
      username: state.username,
      password: state.password,
    };
    signIn(data);
  };
  const onRouter = (args: RoutePath) => {
    props.navigation.navigate(args);
  };
  return (
    <KeyboardAwareScrollView>
      <LinearGradient colors={['#00b09b', '#00b09b']}>
        <View style={authenticateStyles.disply_column}>
          <View>
            <Text style={authenticateStyles.title}>Graf</Text>
          </View>
          <View>
            <View style={{...authenticateStyles.group, marginBottom: 30}}>
              <View style={authenticateStyles.bIcon}>
                <Image style={authenticateStyles.icon} source={googlePng} />
              </View>
              <View style={authenticateStyles.bIcon}>
                <Image style={authenticateStyles.icon} source={facebookPng} />
              </View>
            </View>
            <View style={authenticateStyles.field}>
              <FontAweomse
                style={authenticateStyles.iconInput}
                name="user"
                size={25}
              />
              <TextInput
                style={authenticateStyles.input}
                autoCompleteType="name"
                onChange={onChangeUsername}
                placeholder="Username"
              />
            </View>
            <View style={authenticateStyles.field}>
              <FontAweomse
                style={authenticateStyles.iconInput}
                name="lock"
                size={25}
              />
              <TextInput
                style={authenticateStyles.input}
                autoCompleteType="password"
                onChange={onChangePassword}
                placeholder="Password"
                secureTextEntry={true}
              />
            </View>
            <Button style={authenticateStyles.button} onPress={onSubmit}>
              <Text style={authenticateStyles.textButton}>Sign in</Text>
            </Button>
            <View style={authenticateStyles.group}>
              <TouchableOpacity onPress={onRouter.bind('', 'register')}>
                <Text style={authenticateStyles.textGroup}>
                  create new accounts?
                </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={onRouter.bind('', 'forgot')}>
                <Text style={authenticateStyles.textGroup}>
                  forgotted password?
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <Text style={authenticateStyles.textAlign}>© 2021 GitHub, Inc.</Text>
        </View>
      </LinearGradient>
    </KeyboardAwareScrollView>
  );
};

export default LoginComponent;
