export const nameIconArray = [
  {
    id: 0,
    name: 'home',
    color: '#00b09b',
    router: 'home',
  },
  {id: 1, name: 'search', color: '#00b09b'},
  {id: 2, name: 'plus', color: '#00b09b'},
  {id: 3, name: 'heart', color: '#00b09b', router: 'notification'},
  {id: 4, name: 'user-circle', color: '#00b09b', router: 'settings'},
];

export const choiceIconArray = ['times', 'grin-hearts', 'heart'];

export const choiceImage = [
  'https://static.wikia.nocookie.net/doraemon/images/c/c6/Nobita_Nobi_-_1979_anime.png/revision/latest?cb=20201020233342&path-prefix=en',
  'https://pm1.narvii.com/6816/46ea57eefeb894563715b393479d4d27fb70994dv2_hq.jpg',
  'https://4.bp.blogspot.com/-hqGvBr_VkSg/UMoG3x9Td2I/AAAAAAAAAZw/zW_GbGSJGMo/s1600/doraemon.gif',
];
