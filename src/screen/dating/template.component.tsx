import {Button, Text, View} from 'native-base';
import React from 'react';
import {datingStyle} from './dating';
import FontAwesome from 'react-native-vector-icons/FontAwesome5';
import _ from 'lodash';
import {choiceIconArray, choiceImage, nameIconArray} from './child/icon';
import {Image} from 'react-native';
import {RoutePath, ParamsListProps} from '../../routes';
import {StackNavigationProp} from '@react-navigation/stack';

type RootProp = StackNavigationProp<ParamsListProps>;

interface NavigationProp {
  navigation: RootProp;
}

interface ChoiceProp {
  id?: number;
  name?: string;
}

const TemplateComponent: React.FC<NavigationProp> = (
  props: React.PropsWithChildren<NavigationProp>,
) => {
  const [choice, setChoice] = React.useState<ChoiceProp>({
    id: 0,
    name: 'home',
  });
  const [_choiceImage, _setChoiceImage] = React.useState<number>(1);
  const onChoice = (id: number, name: RoutePath) => {
    setChoice({
      ...choice,
      id,
      name,
    });
    props.navigation.push('profile', {
      name: 'Hai This is my name',
    });
  };

  const handleClickChoice = () => {
    const _choice = _choiceImage + 1;
    _setChoiceImage(_choice);
  };
  return (
    <View style={datingStyle.root}>
      <View
        style={{
          height: '85%',
        }}>
        {_.map(
          choiceImage.slice(_choiceImage - 1, _choiceImage),
          (base, index) => (
            <Image
              style={{
                width: '100%',
                height: '100%',
                resizeMode: 'cover',
                flex: 1,
                // width: Dimensions.get('screen').width,
                // height: Dimensions.get('screen').height / 1.5,
              }}
              resizeMode="cover"
              key={index}
              source={{
                uri: base,
              }}
            />
          ),
        )}
        <View style={datingStyle.choiceGroup}>
          {_.map(choiceIconArray, (base, index) =>
            index === 1 ? (
              <Button key={index} style={datingStyle.choiceSayHello}>
                <FontAwesome name={base} size={25} />
                <Text style={datingStyle.choiceSayHelloText}>Say Hello</Text>
              </Button>
            ) : (
              <Button
                key={index}
                style={datingStyle.choiceButton}
                onPress={index === 2 ? handleClickChoice : undefined}>
                <FontAwesome name={base} size={25} />
              </Button>
            ),
          )}
        </View>
      </View>
      <View style={datingStyle.navbarBottom}>
        {_.map(nameIconArray, (base, index) => (
          <Button
            style={
              choice.id === base.id
                ? {...datingStyle.buttonBottom, backgroundColor: '#00b09b'}
                : datingStyle.buttonBottom
            }
            key={index}
            onPress={onChoice.bind(base, base.id, 'profile')}>
            <FontAwesome
              name={base.name}
              size={25}
              style={
                choice.id === base.id
                  ? {
                      color: 'white',
                    }
                  : {color: base.color}
              }
            />
          </Button>
        ))}
      </View>
    </View>
  );
};

export default TemplateComponent;
