import {Reducer} from 'redux';
import {UserState, UserTypes} from '../types/userTypes';

const initialState: UserState = {
  token: null,
  user: {},
  userAll: [],
  loading: false,
};

export const userReducer: Reducer<UserState> = (
  state = initialState,
  action,
) => {
  switch (action.type) {
    case UserTypes.LoginIn:
      return {
        ...state,
        token: action.payload.token,
      };
      break;
    case UserTypes.SignOut:
      return {
        ...state,
        token: '',
      };
      break;
    default:
      return state;
      break;
  }
};
