export interface ChangeEmailState {
  oldEmail: string;
  newEmail: string;
  password: string;
}

export interface ChangePasswordState {
  oldPassword: string;
  newPassword: string;
  confirmPassword: string;
}
