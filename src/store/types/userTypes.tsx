import {User} from './interface';

export enum UserTypes {
  LoginIn = 'LoginIn',
  SignOut = 'SignOut',
}

export interface UserState {
  readonly token: any;
  readonly user: User;
  readonly userAll: User[];
  readonly loading: boolean;
}
